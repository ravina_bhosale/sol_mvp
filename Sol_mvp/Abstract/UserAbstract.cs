﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract
{
    public abstract class UserAbstract : IDisposable
    {
        public abstract void Dispose();

        protected abstract Task<IEnumerable<CityEntity>> GetCityDataAsync();

        protected abstract Task<UserEntity> SearchUserDataAsync(UserEntity userEntityObj);
    }
}
