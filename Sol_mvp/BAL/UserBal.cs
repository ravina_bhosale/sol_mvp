﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;

namespace BAL
{
    public class UserBal :UserDal
    {
        protected override Task<IEnumerable<CityEntity>> GetCityDataAsync()
        {
            return base.GetCityDataAsync();
        }

        protected override Task<UserEntity> SearchUserDataAsync(UserEntity userEntityObj)
        {
            return base.SearchUserDataAsync(userEntityObj);
        }

    }
}
