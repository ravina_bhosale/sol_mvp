﻿using Abstract;
using DAL.ORD;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserDal :UserAbstract
    {
        #region Declaration
        private UserDcDataContext dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            dc = new UserDcDataContext();
        }
        #endregion

        #region Private Methods
        private async Task<IEnumerable<HobbyEntity>> GetUserHobbyDataAsync(HobbyEntity userHobbeEntityObj)
        {
            try
            {
                return await Task.Run(() =>
                {

                    var getHobbyDataQuery =
                        dc
                        ?.uspGetUsers(
                            "UserHobbyData",
                            userHobbeEntityObj?.UserId
                        )
                        ?.AsEnumerable()
                        ?.Select((leUspGetUserResultSetObj) => new HobbyEntity()
                        {
                            HobbyId = leUspGetUserResultSetObj?.HobbyId,
                            HobbyName = leUspGetUserResultSetObj?.HobbyName,
                            Interested = leUspGetUserResultSetObj?.Interested
                        })
                        ?.ToList();

                    return getHobbyDataQuery;
                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Public Methods
        public override void Dispose()
        {
            dc?.Dispose();
            dc = null;
            GC.SuppressFinalize(this);
        }

        protected async override Task<IEnumerable<CityEntity>> GetCityDataAsync()
        {
            try
            {
                return await Task.Run(() => {

                    var getCityData =
                        dc
                        ?.uspGetCity()
                        ?.AsEnumerable()
                        ?.Select((leUspGetCityResultSetObj) => new CityEntity()
                        {
                            CityId = leUspGetCityResultSetObj.CityId,
                            CityName = leUspGetCityResultSetObj.CityName
                        })
                        ?.ToList();

                    return getCityData;
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected async override Task<UserEntity> SearchUserDataAsync(UserEntity userEntityObj)
        {
            try
            {
                return await Task.Run(() => {

                    var getUserDataQuery =
                        dc
                        ?.uspGetUsers(
                            "UserData",
                            userEntityObj?.UserId
                            )
                            ?.AsEnumerable()
                            ?.Select((leUspGetUserResultSetObj) => new UserEntity()
                            {
                                UserId = leUspGetUserResultSetObj.UserId,
                                FirstName = leUspGetUserResultSetObj.FirstName,
                                LastName = leUspGetUserResultSetObj.LastName,
                                BirthDate = (leUspGetUserResultSetObj?.BirthDate)?.ToString(),
                                Age = leUspGetUserResultSetObj?.Age,
                                Gender = leUspGetUserResultSetObj?.Gender,
                                CityId = leUspGetUserResultSetObj?.CityId,
                                UserHobby = this.GetUserHobbyDataAsync(
                                        new HobbyEntity()
                                        {
                                            UserId = userEntityObj?.UserId
                                        }
                                        ).Result as List<HobbyEntity>
                            })
                            .ToList()
                            ?.FirstOrDefault();

                    return getUserDataQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
