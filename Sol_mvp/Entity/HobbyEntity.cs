﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class HobbyEntity
    {
        public decimal? HobbyId { get; set; }

        public string HobbyName { get; set; }

        public bool? Interested { get; set; }

        public decimal? UserId { get; set; }
    }
}
