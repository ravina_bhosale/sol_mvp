﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public bool? Gender { get; set; }

        public String BirthDate { get; set; }

        public int? Age { get; set; }

        public decimal? CityId { get; set; }

        public List<HobbyEntity> UserHobby { get; set; }
    }
}
