﻿using BAL;
using Entity;
using Sol_mvp.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_mvp.Presenter
{
    public class UserPresenter :UserBal
    {
        #region Declaration
        private IUserView _userView = null;
        #endregion

        #region Constructor
        public UserPresenter()
        {

        }

        public UserPresenter(IUserView userView)
        {
            this._userView = userView;
        }
        #endregion

        #region private method
        private async Task BindCity()
        {
            _userView.CityBinding = await this.GetCityDataAsync();
        }

        private async Task DataMapping(UserEntity userEntityObj)
        {
            await Task.Run(() => {

                _userView.FirstNameBinding = userEntityObj.FirstName;
                _userView.LastNameBinding = userEntityObj.LastName;
                _userView.GenderBinding = Convert.ToBoolean(userEntityObj.Gender);
                _userView.DobBinding = userEntityObj.BirthDate;
                _userView.AgeBinding = (userEntityObj.Age).ToString();
                _userView.HobbyBinding = userEntityObj.UserHobby;
                _userView.CityBinding = Convert.ToString(userEntityObj.CityId);

            });
        }
        #endregion

        #region Public Method

        public async Task OnLoadDataBind()
        {
            var userEntityObj = await base.SearchUserDataAsync(new UserEntity()
            {
                UserId = Convert.ToInt32(_userView.SearchBinding)
            });

            await DataMapping(userEntityObj);
        }
        #endregion 
    }
}