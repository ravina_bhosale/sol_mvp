﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_mvp.View
{
    public interface IUserView
    {
        String SearchBinding { get; set; }

        String FirstNameBinding { get; set; }

        String LastNameBinding { get; set; }

        String DobBinding { get; set; }

        String AgeBinding { get; set; }

        Boolean GenderBinding { get; set; }

        dynamic CityBinding { get; set; }

        List<HobbyEntity> HobbyBinding { get; set; }
    }
}
