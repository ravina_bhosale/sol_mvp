﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="UserPage.aspx.cs" Inherits="Sol_mvp.View.UserPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>

                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                            </td>

                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtFirstName" runat="server" placeholder="FirstName"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtLastName" runat="server" placeholder="LastName"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="lblHobbies" runat="server" Text="Hobbies : "></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBoxList ID="chkHobbies" runat="server" DataTextField="HobbyName" DataValueField="HobbyId">
                                </asp:CheckBoxList>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtBirthDate" runat="server" TextMode="Date" AutoPostBack="true"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblAge" runat="server"></asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="lblGender" runat="server" Text="Gender :"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbGender" runat="server">
                                    <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Female" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="ddlCity" runat="server" DataTextField="CityName" DataValueField="CityId"></asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>

                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </form>
</body>
</html>
