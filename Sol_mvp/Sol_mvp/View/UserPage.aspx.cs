﻿using Sol_mvp.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_mvp.View
{
    public partial class UserPage : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack==false)
            {
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }

        protected async void btnSearch_Click(object sender, EventArgs e)
        {
            // Create an Instance of Presenter
            UserPresenter userPresnterObj = new UserPresenter(this);
            await userPresnterObj.OnLoadDataBind();
        }
    }
}