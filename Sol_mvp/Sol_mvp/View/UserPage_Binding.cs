﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entity;
using System.Web.UI.WebControls;

namespace Sol_mvp.View
{
    public partial class UserPage : IUserView
    {
        public string AgeBinding
        {
            get
            {
                return
                    lblAge.Text;
            }
            set
            {
                lblAge.Text = value;
            }
        }

        public dynamic CityBinding
        {
            get
            {
                return
                    ddlCity.SelectedValue;
            }
            set
            {
                if (value is List<CityEntity>) 
                {
                    List<CityEntity> listCityEntityObj = value as List<CityEntity>;
                    ddlCity.DataSource = listCityEntityObj;
                    ddlCity.DataBind();
                }
                else if (value is String) 
                {
                    ddlCity.SelectedValue = value;
                }


                //ddlCity.AppendDataBoundItems = true;
                //ddlCity.Items.Insert(0, new ListItem("--Select City--", "0"));
                //ddlCity.SelectedIndex = 0;
            }
        }

        public string DobBinding
        {
            get
            {
                return
                    txtBirthDate.Text;
            }
            set
            {
                txtBirthDate.Text = value;
            }
        }

        public string FirstNameBinding
        {
            get
            {
                return Server.HtmlEncode(txtFirstName.Text.Trim());
            }
            set
            {
                txtFirstName.Text = Server.HtmlDecode(value.Trim());
            }
        }

        public bool GenderBinding
        {
            get
            {
                return
                    (rbGender
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.Where((leListItemObj) => leListItemObj.Selected == true)
                    ?.FirstOrDefault()
                    ?.Text) == "Male" ? true : false;
            }
            set
            {
                var flag = value;

                rbGender
                   ?.Items
                   ?.Cast<ListItem>()
                   ?.AsEnumerable()
                   ?.ToList()
                   ?.ForEach((leListItemObj) => {

                       if (flag == true)
                       {
                           if (leListItemObj.Text == "Male")
                           {
                               leListItemObj.Selected = true;
                           }
                           else
                           {
                               leListItemObj.Selected = false;
                           }
                       }
                       else
                       {
                           if (leListItemObj.Text == "Female")
                           {
                               leListItemObj.Selected = true;
                           }
                           else
                           {
                               leListItemObj.Selected = false;
                           }
                       }
                   });
            }
        }

        public List<HobbyEntity> HobbyBinding
        {
            get
            {
                return
                    chkHobbies
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.Select((leListItemObj) => new HobbyEntity()
                    {
                        HobbyName = leListItemObj.Text,
                        Interested = leListItemObj.Selected
                    })
                    .ToList();
            }
            set
            {
                var getUserHobbyEntityObj = value;

                chkHobbies.DataSource = getUserHobbyEntityObj;
                chkHobbies.DataBind();

                // Check Which hobby is Checked.
                chkHobbies
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.ToList()
                    ?.ForEach((leListItemObj) => {

                        leListItemObj.Selected = Convert.ToBoolean(
                                // Check with List
                                getUserHobbyEntityObj
                                ?.AsEnumerable()
                                ?.FirstOrDefault((leUserHobbyEntityObj) => leUserHobbyEntityObj.HobbyName == leListItemObj.Text)
                                ?.Interested
                                );

                    });


            }
        }

        public string LastNameBinding
        {
            get
            {
                return Server.HtmlEncode(txtLastName.Text.Trim());
            }
            set
            {
                txtLastName.Text = Server.HtmlDecode(value.Trim());
            }
        }

        public string SearchBinding
        {
            get
            {
                return txtSearch.Text;
            }
            set
            {
                txtSearch.Text = value;
            }
        }

      
    }
}